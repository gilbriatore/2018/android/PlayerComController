package br.edu.up.playercomcontroller;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.MediaController;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements OnPreparedListener, MediaController.MediaPlayerControl{
  public static final String AUDIO_FILE_NAME = "audioFileName";
  private static final String TAG = "AudioPlayer";
  private MediaPlayer mediaPlayer;
  private MediaController mediaController;
  private String audioFile;

  private Handler handler = new Handler();

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);


    String arquivo = "android.resource://br.edu.up.playercomcontroller/raw/talking_heads_psycho_killer";

    //audioFile = this.getIntent().getStringExtra(AUDIO_FILE_NAME);
    audioFile = arquivo;


    ((TextView)findViewById(R.id.now_playing_text)).setText(audioFile);

    //mediaPlayer = new MediaPlayer();
    mediaPlayer = MediaPlayer.create(this, R.raw.talking_heads_psycho_killer);
    mediaPlayer.setOnPreparedListener(this);

    mediaController = new MediaController(this);
    //mediaController.setVisibility(View.VISIBLE);


    mediaPlayer.start();
    mediaController.show(500000);

//    try {
//      //mediaPlayer.setDataSource(this, Uri.parse(audioFile));
//      mediaPlayer.prepare();
//      mediaPlayer.start();
//    } catch (IOException e) {
//      Log.e(TAG, "Could not open file " + audioFile + " for playback.", e);
//    }

  }

  @Override
  protected void onStop() {
    super.onStop();
    mediaController.hide();
    mediaPlayer.stop();
    mediaPlayer.release();
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    //the MediaController will hide after 3 seconds - tap the screen to make it appear again
    mediaController.show();
    return false;
  }

  //--MediaPlayerControl methods----------------------------------------------------
  public void start() {
    mediaPlayer.start();
  }

  public void pause() {
    mediaPlayer.pause();
  }

  public int getDuration() {
    return mediaPlayer.getDuration();
  }

  public int getCurrentPosition() {
    return mediaPlayer.getCurrentPosition();
  }

  public void seekTo(int i) {
    mediaPlayer.seekTo(i);
  }

  public boolean isPlaying() {
    return mediaPlayer.isPlaying();
  }

  public int getBufferPercentage() {
    return 0;
  }

  public boolean canPause() {
    return true;
  }

  public boolean canSeekBackward() {
    return true;
  }

  public boolean canSeekForward() {
    return true;
  }

  @Override
  public int getAudioSessionId() {
    return 0;
  }
  //--------------------------------------------------------------------------------

  public void onPrepared(MediaPlayer mediaPlayer) {
    Log.d(TAG, "onPrepared");
    mediaController.setMediaPlayer(this);
    mediaController.setAnchorView(findViewById(R.id.main_audio_view));

    handler.post(new Runnable() {
      public void run() {
        mediaController.setEnabled(true);
        mediaController.show();
      }
    });
  }
}
