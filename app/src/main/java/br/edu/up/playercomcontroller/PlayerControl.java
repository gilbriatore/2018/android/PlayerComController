package br.edu.up.playercomcontroller;

import android.media.MediaPlayer;
import android.widget.MediaController;

public class PlayerControl implements MediaController.MediaPlayerControl {

  private MediaPlayer player;

  public PlayerControl(MediaPlayer player){
    this.player = player;
  }

  @Override
  public void start() {
    player.start();
  }

  @Override
  public void pause() {
    player.pause();
  }

  @Override
  public int getDuration() {
    return player.getDuration();
  }

  @Override
  public int getCurrentPosition() {
    return player.getCurrentPosition();
  }

  @Override
  public void seekTo(int i) {
    player.seekTo(i);
  }

  @Override
  public boolean isPlaying() {
    return player.isPlaying();
  }

  @Override
  public int getBufferPercentage() {
    return 0;
  }

  @Override
  public boolean canPause() {
    return false;
  }

  @Override
  public boolean canSeekBackward() {
    return false;
  }

  @Override
  public boolean canSeekForward() {
    return false;
  }

  @Override
  public int getAudioSessionId() {
    return 0;
  }
}
